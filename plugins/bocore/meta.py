# -*- coding: utf-8 -*-

import logging
from sqlite3 import dbapi2 as sqlite

from sqlalchemy import create_engine

from bopress.hook import add_action

__author__ = 'yezang'


def db_engine():
    return create_engine("sqlite:///bopress.sqlite3", echo=True, encoding="utf-8", module=sqlite)
    # return create_engine("mysql+pymysql://root:@127.0.0.1:3306/bopress?charset=utf8",
    #                      echo=True, encoding="utf-8", pool_size=20, max_overflow=5, pool_recycle=3600)


def conf(settings):
    settings.DEBUG = True
    settings.LOGGER_LEVEL = logging.ERROR
    settings.DBEngine = db_engine


add_action("bo_settings", conf)
